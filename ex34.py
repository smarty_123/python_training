animals = ['bear', 'python', 'peacock', 'kangaroo', 'whale', 'platypus']
# The animal at 1.
print(animals[0])
#  2. The third animal.
print(animals[2])
#  3. The fi rst animal.
print(animals[0])
#  4. The animal at 3.
print(animals[2])
#  5. The fifth animal.
print(animals[4])
#  6. The animal at 2.
print(animals[1])
#  7. The sixth animal.
print(animals[5])
# 8. The animal at 4.
print(animals[3])
