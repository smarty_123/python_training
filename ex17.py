import os.path
print("Copying from file1_ex17 to file2_ex17")
# we could do these two on one line too, how?
in_file = open("file1_ex17.txt","r")
indata = in_file.read()

print ("The input file is %d bytes long" % len(indata))

print ("Does the output file exist? %r" % os.path.exists("file2_ex17"))
print ("Ready, hit RETURN to continue, CTRL- C to abort.")
input("?")

out_file = open("file2_ex17.txt", 'w')
out_file.write(indata)

print ("Alright, all done.")

out_file.close()
in_file.close()