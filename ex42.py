## Animal is- a object (yes, sort of confusing) look at the extra credit
class Animal(object):
   pass

## Dog is-a Animal 
class Dog(Animal):

 def __init__(self, name):
   ## ??
    self.name = name

## Cat is-a Animal
class Cat(Animal):

  def __init__(self, name):
    ## ??
    self.name = name

## Person is-a object
class Person(object):

  def __init__(self, name):
   ## ??
   self.name = name

   ## Person has- a pet of some kind
   self.pet = None

## Employee is-a person
class Employee(Person):

  def __init__(self, name, salary):
   ## ?? Employee has-a name
   super(Employee, self).__init__(name)
   ## Employee has-a salary
   self.salary = salary

## Fish is-a object
class Fish(object):
 pass

## Salmon is-a fish
class Salmon(Fish):
 pass

## Halibut is-a fish
class Halibut(Fish):
 pass


## rover is- a Dog
rover = Dog("Rover")

## Satan is-a cat
satan = Cat("Satan")

## Mary is-a Person
mary = Person("Mary")

## mary has-a pet named Satan
mary.pet = satan

## Employee has-a name Frank and salary 120000
frank = Employee("Frank", 120000)
## frank has-a pet named rover
frank.pet = rover
print("hello hi byeeeeeeee")
## flipper is-a fish
flipper = Fish()
## crouse is-a Salmon
crouse = Salmon()
## harry is-a Halibut
harry = Halibut()